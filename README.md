# Dom Creator

It is a simple and small library, which facilitates the creation of objects dynamically through javascript.

## Installation

You just have to call the domCreator.min.js file inside the script tag

`<script src="domCreator.min.js"></script>`

## Documentation
To create a new object, the constructor requires

* Type: of the HTML object. Example: p, img, div.
* Parent: DOM node object. Example: document.body
* Id: DOM selector only if necessary, by default null.
* ClassName: class name of the style sheet. By default null.

```javascript
var div = new DomCreator.Element('div', document.body, null, null);
 
//HTM Dom Elment
div.element
    
//node parent
div.parent
```
## Methods of the object Element

* setClass(Class Name)
* addClass(Class Name)
* removeClass(Class Name)
* addAtributte(type, value)		
* addEvents(types, callback)
* removeEvents (types, callback)
* addText(text) 
* getValue()
* addValue(value)
* addName(name)
* removeElement()
* addDivParent()
* addChild(type)
* insertBefore(element)* 

## Other functions
### AJAX 
* formatObject (sendData) // format an object in formData
* reqFromServer (method, url, sendData, responseType, requestHeader) // full ajax call
* reqSimpleFromServer (method, url, sendData) // call ajax

### Edit DOM element 
* removeAllChild(parentElement)
* removeSelf(element)

### Browser functions
* isOnline () // available network
* isDevice () // Returns true if it is a mobile agent
* getBrowser () // returns the current browser
* typeOfDevice () // returns an object formed by the browser, its version and the type of device.

### Other functions
* randomColor () // returns a random color code
* storageAvailable (type) // returns if the browser has support for localStorage

### dynamic load of resources
* loadJS (url)
* loadCSS (url)
# MIT License
